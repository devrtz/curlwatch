#!/usr/bin/env python3
# Copyright 2023 Evangelos Ribeiro Tzaras
# SPDX-License-Identifier: GPL-3.0-or-later

import configparser
import pycurl
import re
import sys
from io import BytesIO
from urllib.parse import urlparse


DEFAULT_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) '\
    'Gecko/20100101 Firefox/115.0'
# Default encoding for HTML is iso-8859-1.
DEFAULT_ENCODING = 'iso-8859-1'
encoding = None


def handle_header(header_line: bytes):
    global encoding
    header_line = header_line.decode(DEFAULT_ENCODING)

    if ':' not in header_line:
        return

    name, value = header_line.split(':', 1)

    name = name.strip()
    value = value.strip()
    name = name.lower()
    if name == 'content-type':
        content_type = value.lower()
        match = re.search(r'charset=(\S+)', content_type)
        if match:
            encoding = match.group(1)


def retrieve_html(url: str) -> str:
    global encoding
    encoding = None
    c = pycurl.Curl()

    c.setopt(pycurl.URL, url)
    c.setopt(pycurl.USERAGENT, DEFAULT_AGENT)
    c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2)
    c.setopt(pycurl.TIMEOUT_MS, 10000)
    c.setopt(pycurl.CONNECTTIMEOUT_MS, 10000)
    c.setopt(pycurl.FOLLOWLOCATION, True)

    buf = BytesIO()
    c.setopt(pycurl.WRITEDATA, buf)
    c.setopt(pycurl.HEADERFUNCTION, handle_header)

    c.perform()
    c.close()

    if encoding is None:
        encoding = DEFAULT_ENCODING

    return buf.getvalue().decode(encoding)


def extract_links_from_html(html: str,
                            top_level_url: str,
                            desc: str,
                            path_fragment: str = ''):
    top_level_url = top_level_url.replace('.', r'\.')
    if not top_level_url.endswith('/'):
        top_level_url += '/'
    regex = r'<a href="(' + top_level_url + path_fragment + r'.+?)".*?>' \
        + desc + r'</a>'

    return re.findall(regex, html)


def generate_for_site(sec: configparser.SectionProxy) -> str:
    url = sec['url']
    html = retrieve_html(url)
    url_parsed = urlparse(url)
    top_level = url_parsed.scheme + '://' + url_parsed.hostname

    desc = sec['desc']
    fragment = sec['path-fragment'] if 'path-fragment' in sec.keys() else ''
    links = extract_links_from_html(html, top_level, desc, fragment)

    if 'n' in sec.keys():
        n = sec.getint('n')
        if n < len(links):
            links = links[:n]

    cfg = ''
    remove_numbered = re.compile(r'-[0-9]+$')
    for link in links:
        suffix = link.split('/')[-1]
        suffix = remove_numbered.sub('', suffix)
        cfg += f'[{sec.name}-{suffix}]\n'
        cfg += f'url = {link}\n\n'

    return cfg


def generate_for_cfg(fname: str) -> str:
    cfg = configparser.ConfigParser()
    with open(fname, 'r') as f:
        cfg.read_file(f)

    gen_cfg = ''
    for sec in cfg.sections():
        gen_cfg += generate_for_site(cfg[sec])

    return gen_cfg[:-1]


if __name__ == '__main__':
    cfg = ''
    for f in sys.argv[1:]:
        cfg += generate_for_cfg(sys.argv[1])

    print(cfg)
