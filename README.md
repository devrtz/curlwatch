# Monitor homepage for changes

curlwatch is a small program that you can use to monitor a number of URLs.

## Dependencies

You need python3 and pycurl installed. On a Debian system run
```
apt install python3-pycurl
```

and you're good to go.

## Running curlwatch

You can either pass URLs on the command line `./watch.py http://example.org`
or retrieve them from a configuration file `./watch.py -c my.conf`.

The script will fetch the URLs a given number of times (use `-m` or `--max-requests` arguments,
or the `max-requests` key your configuration) and save it to the output directory (defaults to `out/`).
All retrieved headers and bodies will be saved with the (unwieldy) prefix
`<TIMESTAMP>_<RUNNER>_[INTERFACE]_[IP4/6]_<SITE>-<REQUEST_COUNTER>-<REDIRECT_COUNTER>`

### Configuration file

Default options that apply to all sites to watch are specified in the `[DEFAULT]` section.
Every site you want to watch goes into it's own section and needs the `url` key set.
You can also set site-specific overrides, if you want to deviate from the defaults.
Command line arguments can also be used to override the settings from the configuration.

```
[DEFAULT]
force-ipv4 = False
force-ipv6 = False
retry-fail = True
retry-fail-count = 3
retry-sleep = 60
watch-interval = 600
randomize-interval = 300
useragent = Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0
follow-redirects = True
max-redirects = 20
max-requests = 0
verbose = False
duplicate-jobs = 0

[example]
url = https://example.org
```

### Notable commandline options

To randomize used user agents, you can specify `-U` and point it to a tab separated file containing
`USERAGENT <TAB> WEIGHT` lines. You can download such files from [useragents.me](https://useragents.me).

## Analysis

The [analysis script](./extract.py) is currently pretty bare-bones.
You specify the directory to check and give it a config used previously for `watch.py`.
It will then checksum all files for the sites in the config.
If the body of a site was not always the same, it will print the checksum and file.

See the following example run:

```
> $ ./extract.py -d test_run -c watch.conf
Found 4177 .body filenames containing "test-site"
Sha1 "a216d18da3ab9a4611f068c713811b49ccda7800" found 4175 times, e.g. "test_run/20230726_154726-runner1-test-site_00000-00.body"
Sha1 "a02c8009b918a1fdfbe09e8528bbd4165985eef3" found 2 times, e.g. "test_run/20230725_190127-runner2-test-site-dup04_00124-00.body"
```

You can then inspect the differences further, e.g. using `diff`.

## Config (fragment) generator

The links you want to watch for might be coming from some link list.
You can use [generate.py](./generate.py) to fetch a "top level" URL
extract all links matching some criteria, e.g. link description
and generate a config fragment from that.

### Example config

To generate the fragment you need to provide a configuration in the following form.
```
[DEFAULT]
path-fragment = to/
n = 15

[my-site]
url = https://example.org
desc = Click here
```
For any given site (here `my-site`) it will fetch the url and look for any links
that roughly match `<a href="$URL/$PATH_FRAGMENT">$DESCRIPTION</a>`
and output every result to stdout.

# License
All code written is licensed under GPL3+, see [COPYING](./COPYING).
