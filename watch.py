#!/usr/bin/env python3
# Copyright 2023 Evangelos Ribeiro Tzaras
# SPDX-License-Identifier: GPL-3.0-or-later

import optparse
import configparser
import sys
import os
import pycurl
import re
import datetime
import logging
import signal
import time
import random
from io import BytesIO, TextIOWrapper

now = datetime.datetime.now(tz=datetime.timezone.utc)
local_tz = now.astimezone().tzinfo
DEFAULT_PREFIX = now.strftime('%Y%m%d_%H%M%S')
now = datetime.datetime.now(tz=local_tz)

# Default encoding for HTML is iso-8859-1.
DEFAULT_ENCODING = 'iso-8859-1'
DEFAULT_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) '\
    'Gecko/20100101 Firefox/115.0'

log = logging.getLogger(__name__)

DEFAULT_CONFIG = {
    'force-ipv4': False,
    'force-ipv6': False,
    'retry-fail': False,
    'retry-fail-count': 3,
    'retry-sleep': 20,
    'watch-interval': 600,
    'randomize-interval': 300,
    'max-requests': 0,
    'useragent': DEFAULT_AGENT,
    'follow-redirects': False,
    'max-redirects': 20,
    'verbose': False,
}


class URLRequest:
    def __init__(self, url: str, user_agent: str, extra_opts: dict = {}):
        self._url = url
        self._user_agent = user_agent
        self._extra_opts = extra_opts

        self._c = pycurl.Curl()
        self._c.setopt(pycurl.URL, url)
        self._c.setopt(pycurl.USERAGENT, user_agent)
        self._c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2)
        self._c.setopt(pycurl.TIMEOUT_MS, 10000)
        self._c.setopt(pycurl.CONNECTTIMEOUT_MS, 10000)
        for opt, val in extra_opts.items():
            self._c.setopt(opt, val)

        self._has_run = False

    @property
    def url(self) -> str:
        """The set URL"""
        return self._url

    @url.setter
    def url(self, url: str):
        self._url = url
        self._c.setopt(pycurl.URL, url)

    @property
    def user_agent(self) -> str:
        """The set user agent"""
        return self._user_agent

    @property
    def extra_opts(self) -> dict:
        """A dictionary of extra options given to pycurl"""
        return self._extra_opts

    @property
    def status_code(self) -> int:
        """The status code of the response"""
        return self._c.getinfo(pycurl.RESPONSE_CODE)

    @property
    def error(self) -> bool:
        """Whether any error occured"""
        return self.status_code > 400

    @property
    def headers(self) -> dict:
        """The response headers"""
        return self._headers if self._has_run else None

    @property
    def headers_raw(self) -> bytes:
        """The raw response header"""
        return self._headers_raw if self._has_run else None

    @property
    def redirect(self) -> str:
        """The URL to be redirected to"""
        if (self.status_code == 301
            or self.status_code == 302
            or self.status_code == 307
            or self.status_code == 308) and \
                'location' in self.headers:

            return self.headers['location']

        return None

    @property
    def body_raw(self) -> bytes:
        """The raw body of the response"""
        return self._buffer.getvalue() if self._has_run else None

    @property
    def body(self) -> str:
        """The body of the response"""
        encoding = None
        if 'content-type' in self.headers:
            content_type = self.headers['content-type'].lower()
            match = re.search(r'charset=(\S+)', content_type)
            if match:
                encoding = match.group(1)

        if encoding is None:
            encoding = DEFAULT_ENCODING

        return self.body_raw.decode(encoding) if self._has_run else None

    @property
    def set_cookies(self) -> list:
        """A list of cookies the server has requested to set in the response"""
        cookies = None
        if 'set-cookie' in self.headers:
            if isinstance(self.headers['set-cookie'], list):
                cookies = self.headers['set-cookie']
            else:
                cookies = [self.headers['set-cookie']]

        return cookies if self._has_run else None

    def handle_header(self, header_line: bytes):
        self._headers_raw += header_line
        header_line = header_line.decode(DEFAULT_ENCODING)

        if ':' not in header_line:
            return

        name, value = header_line.split(':', 1)

        name = name.strip()
        value = value.strip()
        name = name.lower()

        # don't overwrite if header is given multiple times
        if name in self._headers:
            if isinstance(self.headers[name], list):
                self.headers[name].append(value)
            # discard duplicate headers when following redirects
            elif self.headers[name] == value:
                return
            else:
                self.headers[name] = [self.headers[name], value]
        else:
            self._headers[name] = value

    def run(self):
        self._headers = dict()
        self._headers_raw = b''
        self._buffer = BytesIO()

        self._c.setopt(pycurl.WRITEDATA, self._buffer)
        self._c.setopt(pycurl.HEADERFUNCTION, self.handle_header)

        self._c.perform()
        self._has_run = True

    def close(self):
        self._c.close()


class Site:
    def __init__(self,
                 out_prefix: str,
                 cfg: configparser.SectionProxy):
        self._name = cfg.name
        self._prefix = f'{out_prefix}-{self._name}'
        self._url = cfg['url']
        self._interval = cfg.getint('watch-interval')
        self._rand_interval = cfg.getint('randomize-interval')
        self._max_requests = cfg.getint('max-requests')
        self._follow_redirects = cfg.getboolean('follow-redirects')
        self._max_redirects = cfg.getint('max-redirects')
        self._useragent = cfg['useragent']
        self._retry_fail = cfg.getboolean('retry-fail')
        self._retry_fail_count = cfg.getint('retry-fail-count')
        self._retry_sleep = cfg.getint('retry-sleep')
        self._verbose = cfg.getboolean('verbose')
        self._interface = cfg['interface'] \
            if 'interface' in cfg.keys() else None

        ip4 = cfg.getboolean('force-ipv4')
        ip6 = cfg.getboolean('force-ipv6')
        if ip4 and ip6:
            raise ValueError('Cannot force both IPv4 and IPv6')
        if ip4:
            self._ip = pycurl.IPRESOLVE_V4
        elif ip6:
            self._ip = pycurl.IPRESOLVE_V6
        else:
            self._ip = pycurl.IPRESOLVE_WHATEVER

        # FILES
        self._cookie_file = f'{self._prefix}.cookie'
        self._report_file = f'{self._prefix}-report.json'  # TODO
        self._header_files = []
        self._body_files = []

        # timing logic
        self._next_run = datetime.datetime.now(tz=local_tz)

        # extract options for URLRequest
        opts = dict()
        opts[pycurl.IPRESOLVE] = self._ip
        if self._useragent:
            opts[pycurl.USERAGENT] = self._useragent
        if self._verbose:
            opts[pycurl.VERBOSE] = True
        if self._interface:
            opts[pycurl.INTERFACE] = self._interface

        opts[pycurl.COOKIEJAR] = self._cookie_file
        opts[pycurl.COOKIEFILE] = self._cookie_file

        self._req = URLRequest(self._url, self._useragent, opts)

        # other
        self._counter = 0
        self._inactive = False

    @property
    def useragent(self) -> str:
        return self._useragent

    @property
    def interval(self) -> int:
        return self._interval

    @property
    def rand_interval(self) -> int:
        return self._rand_interval

    @property
    def url(self) -> str:
        return self._url

    @property
    def name(self) -> str:
        return self._name

    @property
    def next_run(self) -> datetime.datetime:
        return self._next_run

    @property
    def max_redirects(self) -> int:
        return self._max_redirects

    @property
    def max_requests(self) -> int:
        return self._max_requests

    @property
    def results(self) -> dict:
        return {
            'report': self._report_file,
            'header': self._header_files,
            'body': self._header_files,
        }

    @property
    def counter(self) -> int:
        return self._counter

    @property
    def inactive(self) -> bool:
        return self._inactive

    def run(self):
        """Fetch site and write logfiles"""
        req_done = False
        log.debug(f"Performing request [{self.counter}] for {self._name}")
        redirect_cnt = 0
        retry_cnt = 0
        self._req.url = self.url

        while not req_done:
            prefix = f'{self._prefix}_{self.counter:05}-{redirect_cnt:02}'
            try:
                self._req.run()
            except pycurl.error as e:
                log.warning(e)
                if not self._retry_fail:
                    log.warning('Not allowed to retry failed request, '
                                'aborting...')
                    break
                if retry_cnt < self._retry_fail_count:
                    retry_cnt += 1
                    log.debug(f'Retry [{retry_cnt}] in {self._retry_sleep} '
                              'seconds')
                    time.sleep(self._retry_sleep)
                    continue
                log.warning('Maximum number of retries reached, aborting...')
                break

            if (self._follow_redirects and self._req.redirect):
                log.debug(f'Redirecting to "{self._req.redirect}"')
                redirect_cnt += 1
                self._req.url = self._req.redirect
            elif (self._follow_redirects and
                  redirect_cnt > self.max_redirects):  # XXX > or >= ??
                log.warning('Maximum number of redirects reached')
                req_done = True
            else:
                req_done = True

            # log headers/html
            fname_header = f'{prefix}.header'
            self._header_files.append(fname_header)
            with open(fname_header, 'wb') as f:
                f.write(self._req.headers_raw)

            if len(self._req.body_raw) == 0:
                continue

            fname_body = f'{prefix}.body'
            self._body_files.append(fname_body)
            with open(fname_body, 'wb') as f:
                f.write(self._req.body_raw)

        self._counter += 1

        if self.max_requests > 0 and self._counter >= self.max_requests:
            log.info(f'All {self.max_requests} requests performed\n'
                     f' Setting site "{self.name}" to inactive')
            self._inactive = True
            return

        if self._req.error:
            log.warning(f'Request was unsuccesful: {self._req.status_code}\n'
                        f' Setting site "{self.name}" to inactive')
            self._inactive = True
            return

        dt = self.interval + random.randint(-self._rand_interval,
                                            self._rand_interval)
        self._next_run = now + datetime.timedelta(seconds=dt)


def agents_from_file(f: TextIOWrapper) -> tuple((list[dict], float)):
    f.seek(0)
    pos = 0
    agents = list()
    sum_weights = 0.0

    while True:
        line = f.readline().strip()
        if line.startswith('#'):
            continue

        if pos == f.tell():
            break

        agent, weight = line.split('\t')
        weight = float(weight)

        agents.append({'agent': agent, 'weight': weight})
        pos = f.tell()
        sum_weights += weight

    return (agents, sum_weights)


def get_random_agent(agents: list[dict], max_weight: float = 100.0) -> str:
    r = random.uniform(0.0, max_weight)
    for agent in agents:
        if r < agent['weight']:
            return agent['agent']
        r -= agent['weight']


def cookie_to_dict(cookie: str) -> dict:
    d = dict()
    for part in cookie.split(';'):
        k, v = part.split('=', 1)
        d[k] = v

    return d


def get_next_site(sites: list[Site]):
    """Retrieve next scheduled site"""
    next_site = None
    for s in sites:
        if s.inactive:
            continue
        if next_site is None or s.next_run < next_site.next_run:
            next_site = s
    return next_site


def override_site_cfg(sec: configparser.SectionProxy,
                      options: optparse.Values):
    # there are 2 default value in options, therefore they are always set
    if len(options.__dict__) == 2:
        return

    log.debug(f'Setting overrides for {sec.name}')
    if options.max_requests:
        sec['max-requests'] = f'{options.max_requests}'
    if options.force_ipv4:
        sec['force-ipv4'] = 'True'
        sec['force-ipv6'] = 'False'
    if options.force_ipv6:
        sec['force-ipv4'] = 'False'
        sec['force-ipv6'] = 'True'
    if options.interval:
        sec['watch-interval'] = f'{options.interval}'
    if options.useragent:
        sec['useragent'] = options.useragent
    if options.follow:
        sec['follow-redirects'] = 'True'
    if options.interval_rand:
        sec['randomize-interval'] = f'{options.interval_rand}'
    if options.interface:
        sec['interface'] = options.interface
    # TODO implement MOAR OVERRIDES


###
# Start of main program
##


usage = \
    "Usage: %prog [options] -c <CONFIG>\n" \
    "   or: %prog [options] <URL>"

parser = optparse.OptionParser(usage)

parser.add_option('-v', '--verbose', dest='verbose',
                  action='store_true',
                  help='Verbose output from curl')
parser.add_option('-d', '--debug', dest='debug',
                  action='store_true',
                  help='Enable debug output')
parser.add_option('-l', '--log', dest='log',
                  action='store_true',
                  help='Whether to mirror logs to file')
parser.add_option('-o', '--out-dir', dest='out_dir', type='string',
                  default='out',
                  help='Directory to write output and logs into [%default]')
parser.add_option('-p', '--prefix', dest='prefix', type='string',
                  default=DEFAULT_PREFIX,
                  help='Prefix for all output files. '
                  'Uses current timestamp by default')
parser.add_option('-n', '--runner-name', dest='runner', type='string',
                  help='Name of this runner. Will be appended to the prefix')
parser.add_option('-4', '--ipv4', dest='force_ipv4',
                  action='store_true',
                  help='Force IPv4 only. Mutually exclusive with IPv6. '
                  'If no IP protocol version is forced on a dual-stack '
                  'host the happy eyeballs algorithm is used. '
                  'Will append "-v4" to the prefix.')
parser.add_option('-6', '--ipv6', dest='force_ipv6',
                  action='store_true',
                  help='Force IPv6 only. Mutually exclusive with IPv4. '
                  'If no IP protocol version is forced on a dual-stack '
                  'host the happy eyeballs algorithm is used. '
                  'Will append "-v6" to the prefix.""')
parser.add_option('-c', '--config', dest='config', type='string',
                  help="A config file to use")
parser.add_option('-i', '--interval', dest='interval', type='int',
                  help='Interval to sleep for between requests')
parser.add_option('-I', '--interface', dest='interface', type='string',
                  help='Which network interface to use')
parser.add_option('-r', '--randomize-interval', dest='interval_rand',
                  type='int',
                  help='Change interval by random value between [-val;+val]')
parser.add_option('-j', '--duplicate-jobs', dest='dup_jobs', type='int',
                  help='Run duplicate jobs')
parser.add_option('-m', '--max-requests', dest='max_requests', type='int',
                  help="Limit the amount of requests")
parser.add_option('-u', '--user-agent', dest='useragent', type='string',
                  help='The user agent to use')
parser.add_option('-U', '--user-agent-file', dest='useragent_file',
                  type='string',
                  help='Use tab separated configuration file '
                  '(e.g. from http://useragent.me) '
                  'to randomize choosen useragents')
parser.add_option('-f', '--follow-redirect', dest="follow",
                  action='store_true',
                  help='Whether to follow redirects')
# TODO cooldown options/pause


options, urls = parser.parse_args()

got_config = options.config is not None
got_urls = len(urls) > 0

if got_config == got_urls or \
   (options.force_ipv4 and options.force_ipv6):
    parser.print_usage()
    sys.exit(1)

if got_urls and not options.max_requests:
    options.max_requests = 1

# Set up logging: Always on console and, if desired, to a logfile
loglevel = logging.DEBUG if options.debug else logging.INFO
formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')

log.setLevel(loglevel)

ch = logging.StreamHandler()
ch.setLevel(loglevel)
ch.setFormatter(formatter)
log.addHandler(ch)

# Setup prefix for all written files
prefix = f'{options.out_dir}/{options.prefix}-'
prefix += options.runner if options.runner else os.uname().nodename
if options.interface:
    prefix += f'-{options.interface}'
if options.force_ipv4:
    prefix += '-v4'
elif options.force_ipv6:
    prefix += '-v6'

os.makedirs(options.out_dir, exist_ok=True)

# Save output to file
if options.log:
    log_path = f'{prefix}.log'
    fh = logging.FileHandler(log_path)
    fh.setLevel(loglevel)
    fh.setFormatter(formatter)
    log.addHandler(fh)

log.info(f'Started as "{" ".join(sys.argv)}"')
log.info(f'Using prefix "{prefix}"')

sites = []
cfg = configparser.ConfigParser(defaults=DEFAULT_CONFIG)

if options.config:
    log.debug(f'Loading configuration from "{options.config}"')
    with open(options.config, 'r') as f:
        cfg.read_file(f)
    log.debug(f'{len(cfg.sections())} site(s) loaded')

for i, url in enumerate(urls):
    name = f'site-{i:02}'
    log.debug(f'Adding "{url}" as "{name}"')
    cfg.add_section(name)
    cfg[name]['url'] = url

useragents = None
useragents = None

if options.useragent_file:
    with open(options.useragent_file) as f:
        useragents = agents_from_file(f)

# duplicate jobs
if options.dup_jobs:
    log.debug(f'Duplicating all sites {options.dup_jobs} times')

    for sec in cfg.sections():
        for i in range(options.dup_jobs):
            name = f'{sec}-dup{i:02}'
            cfg.add_section(name)
            for k, v in cfg[sec].items():
                cfg[name][k] = v

# override configuration from cli args
for sec in cfg.sections():
    override_site_cfg(cfg[sec], options)
    if useragents:
        agent = get_random_agent(useragents[0], useragents[1])
        cfg[sec]['useragent'] = agent

    site = Site(prefix, cfg[sec])
    sites.append(site)
    log.info(f'Added site "{site.name}" [{site.url}] using "{site.useragent}"')


def abort(signum, frame):
    raise SystemExit


signal.signal(signal.SIGTERM, abort)

done = False
while not done:
    now = datetime.datetime.now(tz=local_tz)
    s = get_next_site(sites)
    if s is None:
        done = True
        continue

    log.debug(f'Site "{s.name}" scheduled for {s.next_run}')

    dt = (s.next_run - now).total_seconds()
    try:
        if (dt > 0):
            log.debug(f'Sleeping for {dt:.1f} seconds')
            time.sleep(dt)

        log.debug(f'Fetching "{s.name}"')
        s.run()
    except KeyboardInterrupt:
        log.info('Exiting due to keyboard interrupt')
        done = True
    except SystemExit:
        log.info('Exiting due to caught signal')
        done = True
    except Exception as exc:
        log.info(f'Exiting due to exception "{exc}"')
        done = True

log.info('Done.')
