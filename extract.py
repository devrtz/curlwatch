#!/usr/bin/env python3
# Copyright 2023 Evangelos Ribeiro Tzaras
# SPDX-License-Identifier: GPL-3.0-or-later

import glob
import os
import hashlib
import optparse
import configparser


_DIR = 'out'


def chksum_dict(filenames: list) -> dict:
    chksum_dict = dict()
    for fname in filenames:
        stat = os.stat(fname)
        if stat.st_size == 0:
            continue

        with open(fname, 'rb') as f:
            h = hashlib.sha1(f.read())
            digest = h.hexdigest()
            if digest in chksum_dict:
                files_digest = chksum_dict[digest]
                files_digest.append(fname)
            else:
                chksum_dict[digest] = [fname,]

    return chksum_dict


def glob_files(name: str, directory: str = _DIR) -> list:
    return glob.glob(f'{directory}/*{name}*.body')


##
# Main program
##

parser = optparse.OptionParser()
parser.add_option('-d', '--directory', dest='directory')
parser.add_option('-c', '--config', dest='config')

options, args = parser.parse_args()

directory = options.directory if options.directory else _DIR
cfg = configparser.ConfigParser()
if options.config:
    with open(options.config, 'r') as f:
        cfg.read_file(f)

globs = []
globs.extend(args)
globs.extend(cfg.sections())

print_identical = False

for g in globs:
    files = glob_files(g, directory)
    print(f'Found {len(files)} .body filenames containing "{g}"')
    d = chksum_dict(files)
    if len(d.keys()) == 1 and not print_identical:
        continue

    for sha in d.keys():
        sha_files = d[sha]
        n_sha_files = len(sha_files)
        print(f'Sha1 "{sha}" found {n_sha_files} times, e.g. "{sha_files[0]}"')
        # TODO extract links
    print()
